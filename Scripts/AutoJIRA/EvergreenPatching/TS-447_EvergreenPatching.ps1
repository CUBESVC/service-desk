﻿#########################################################################
# Api call to JIRA to Create an issue on the INF board with related CRs #
#                                                                       #
#                     By Leigh Butterworth                              #
#                           V1.0                                        #
#                                                                       #
#########################################################################
Log-It -custom "Starting"
# Get and set the date variables
$date = (Get-Date).addmonths(1)
$numMonthsAgo = -1
$firstDayOfMonth = Get-Date ((("01/" + (Get-Date $date).Month).ToString() + "/" + ((Get-Date $date).Year).ToString() + " 00:00:00"))
$lastDayOfMonth = (Get-Date ((("01/" + ((($firstDayOfMonth).AddMonths(1)).Month).ToString() + "/" + ((($firstDayOfMonth).AddMonths(1)).Year).ToString())))) - (New-TimeSpan -seconds 1)
$date = Get-Date ($firstDayOfMonth) -UFormat %B%Y
$duedate = Get-Date ($lastDayOfMonth) -UFormat %Y-%m-%d

# Change working directory to the script location
cd $PSScriptRoot

# CR Data
#########

# Master INF Issue
$inf = @"
    {
       "fields":{
          "project":{
             "key":"INF"
          },
          "summary":"CUBE Evergreen $date",
          "description":"CUBE MS Patching.",
          "issuetype":{
             "name":"Request"
          },
          "assignee":{
             "name":"Graham.Case"
          }
       }
    }
"@

# CRs blocked in a stack (CubeDEV blocks CubeTEST blocks CubeUAT blocks CubePROD)
$CRsStacked = @(@"
    {  
       "fields":{  
          "project":{  
             "key":"CR"
          },
          "issuetype":{  
             "name":"Request"
          },
          "duedate":"$duedate",
          "summary":"Evergreen CUBEDEV servers $date",
          "customfield_11700":"Jefree Lim",
          "customfield_11701":[  
             {  
                "value":"Barry Sage"
             },
             {  
                "value":"Colin Ross"
             },
             {  
                "value":"Matt Stanton"
             },
             {  
                "value":"Russell Davis"
             },
             {  
                "value":"Graham Case"
             }
          ],
          "description":"Patch CUBEDEV servers with latest updates and security from Microsoft",
          "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
          "customfield_11705":{  
             "value":"Yes"
          },
          "customfield_11704":{  
             "value":"DEV"
          },
          "customfield_11709":"CUBEDEVSQL\r\nCUBEDEVIIS\r\nCUBEDEVINX\r\nCUBEDTUPWK01\r\nCUBEDESKTOP06",
          "customfield_11714":"N/A",
          "customfield_11715":"*Backup*\r\n\r\n*  Take a snapshot of the below servers.\r\n#* CUBEDEVSQL\r\n#* CUBEDEVIIS\r\n#* CUBEDEVINX\r\n#* CUBEDTUPWK01\r\n#* CUBEDESKTOP06\r\n\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. Once done go through the above steps for the next VM.\r\n\r\n*Centos Updates*\r\n\r\n* Log in as root.\r\n* Run the following command _su -c 'yum update'_\r\n* Restart the server.\r\n\r\n\r\n* Add CR details to CFG management form and save as next major version\r\n* Restart servers and check services\r\n* If everything is ok delete the snapshots for the servers.",
          "customfield_11716":{  
             "value":"Yes"
          },
          "customfield_11717":"Restore snapshots",
          "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
          "customfield_11801":" ",
          "customfield_11722":{  
             "value":"No"
          },
          "customfield_12026":{  
             "value":"No"
          },
          "customfield_12032":{  
             "value":"Infrastructure"
          },
          "customfield_12033":[  
             {  
                "value":"Evergreen"
             }
          ]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBETEST servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBETEST servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"TEST"},
            "customfield_11709":"CUBETESTSQL\r\nCUBETESTIIS\r\nCUBETESTINX",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n# Take a snapshot of the below servers.\r\n#* CUBETESTSQL\r\n#* CUBETESTIIS\r\n#* CUBETESTINX\r\n\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. \r\n* Once done go through the above steps for the next VM.\r\n\r\n* Add CR details to CFG management form and save as next major version\r\n* Restart servers and check services\r\n* Delete Snapshot",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBEUAT servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBEUAT servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"UAT"},
            "customfield_11709":"CUBEUATSQL\r\nCUBEUATIIS\r\nCUBEUATINX\r\nCUBEDTUELS01\r\nCUBEDTUELS02\r\nCUBEDTUELS03\r\n",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n* Take a snapshot of the below servers.\r\n#* CUBEUATSQL\r\n#* CUBEUATIIS\r\n#* CUBEUATINX\r\n#* CUBEDTUELS01\r\n#* CUBEDTUELS02\r\n#* CUBEDTUELS03\r\n\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. Once done go through the above steps for the next VM.\r\n\r\n*Centos Updates*\r\n\r\n* Log in as root.\r\n* Run the following command _su -c 'yum update'_\r\n* Restart the server.\r\n\r\nAdd CR details to CFG management form and save as next major version\r\nRestart servers and check services\r\nRemove snapshots\r\n",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBEPROD servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBEPROD servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"PROD"},
            "customfield_11709":"CUBEPRODSQL\r\nCUBEPRODIIS\r\nCUBEPRODINX\r\nCUBEPRODELS01\r\nCUBEPRODELS02\r\nCUBEPRODELS03",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n* Take a snapshot of the below servers\r\n#* CUBEPRODSQL\r\n#* CUBEPRODIIS\r\n#* CUBEPRODINX\r\n#* CUBEPRODELS01\r\n#* CUBEPRODELS02\r\n#* CUBEPRODELS03\r\n\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group PROD.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. Once done go through the above steps for the next VM.\r\n\r\n*Centos Updates*\r\n\r\n* Log in as root.\r\n* Run the following command _su -c 'yum update'_\r\n* Restart the server.\r\n\r\nAdd CR details to CFG management form and save as next major version\r\nRestart servers and check services\r\nRemove snapshots\r\n\r\n",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@
)

# Extra environment CRs (CubeSTG, CubePT, CubeDM, CubeTRIAL, CubeOPS, CubeDTUOPS)
$CRs = @(@"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBESTG servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBESTG servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"STG"},
            "customfield_11709":"CUBESTGSQL\r\nCUBESTGIIS\r\nCUBESTGINX",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n\r\n* Take a snapshot of the below servers\r\n#* CUBESTGSQL\r\n#* CUBESTGIIS\r\n#* CUBESTGINX\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. Once done go through the above steps for the next VM.\r\n\r\n\r\n* Add CR details to CFG management form and save as next major version\r\n* Restart servers and check services\r\n* Delete snapshots.",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBEPT servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBEPT servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"PROTOTYPE"},
            "customfield_11709":"CUBEPTSQL\r\nCUBEPTIIS\r\nCUBEPTINX",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n* Take a snapshot of the below servers\r\n#* CUBEPTSQL\r\n#* CUBEPTIIS\r\n#* CUBEPTINX\r\n\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. \r\n* Once done go through the above steps for the next VM.\r\n\r\n\r\n\r\n\r\n* Add CR details to CFG management form and save as next major version\r\n* Restart servers and check services\r\n* Remove snapshots",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBEDM servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBEDM servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"DM"},
            "customfield_11709":"CUBEDMSQL\r\nCUBEDMIIS\r\nCUBEDMINX",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n* Take a snapshot of the below servers\r\n#* CUBEDMSQL\r\n#* CUBEDMIIS\r\n#* CUBEDMINX\r\n\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. \r\n* Once done go through the above steps for the next VM.\r\n\r\n\r\n\r\n\r\n* Add CR details to CFG management form and save as next major version\r\n* Restart servers and check services\r\n* Remove snapshots",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBETRIAL servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBETRIAL servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"TRIAL"},
            "customfield_11709":"CUBETRIALSQL\r\nCUBETRIALIIS\r\nCUBETRIALINX\r\nCUBEBAUELS01\r\nCUBEBAUELS02\r\nCUBEBAUELS03",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n* Take a snapshot of the below servers\r\n#* CUBETRIALSQL\r\n#* CUBETRIALIIS\r\n#* CUBETRIALINX\r\n#* CUBEBAUELS01\r\n#* CUBEBAUELS02\r\n#* CUBEBAUELS03\r\n\r\n*Microsoft Updates*\r\n\r\n* Log on to VM TCG-SVC01\r\n* Open Windows Server Update Services.\r\n* Under \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\n* Select all the updates, right click and select approve. Select the computer group Dev.\r\n* Log on the VM you are working on and launch windows update.\r\n* Select all the required updates and install.\r\n* The server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. \r\n* Once done go through the above steps for the next VM.\r\n\r\n*Centos Updates*\r\n\r\n* Log in as root.\r\n* Run the following command _su -c 'yum update'_\r\n* Restart the server.\r\n\r\nAdd CR details to CFG management form and save as next major version\r\nRestart servers and check services\r\nRemove snapshots",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBE OPS servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBE OPS servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"PROD"},
            "customfield_11709":"CUBE-FS01\r\nCUBEOCTO\r\nCUBEPRODPWK\r\nCUBEPRODSKDN\r\nCUBETEAMCITY\r\nOPS-CUBEVUM\r\nCUBE-DC01\r\nCUBE-DC02",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n# Shut down the below servers and snapshot\r\n#* CUBE-FS01\r\n#* CUBEOCTO\r\n#* CUBEPRODPWK\r\n#* CUBEPRODSKDN\r\n#* CUBETEAMCITY\r\n#* OPS-CUBEVUM\r\n#* CUBE-DC01\r\n#* CUBE-DC02\r\n# Power on servers after snapshot\r\n\r\n*Microsoft Updates*\r\n\r\nLog on to VM TCG-SVC01\r\nOpen Windows Server Update Services.\r\nUnder \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\nSelect all the updates, right click and select approve. Select the computer group Dev.\r\nLog on the VM you are working on and launch windows update.\r\nSelect all the required updates and install.\r\nThe server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. Once done go through the above steps for the next VM.\r\n\r\n*VMWare Tools update.* \r\n\r\nIn VMWare right click the VM to be updated.\r\nSelect guest and choose \"Install/Upgrade VMware Tools\".\r\nSelect \"Automatic Tools Upgrade\". This will install the tools in the background an will reboot the VM once done.\r\n\r\n*CUBEPRODPWK update*\r\nLog into the CUBEPRODPWK as root\r\nRun the following command yum update -y\r\nWait for the updates to be applied and reboot the server.\r\n\r\nAdd CR details to CFG management form and save as next major version\r\n\r\nRestart servers and check services",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@, @"
    {
        "fields": {
            "project": { 
              "key": "CR"
            },
            "issuetype": {
              "name": "Request"
            },
            "duedate":"$duedate",
            "summary":"Evergreen CUBE DTU OPS servers $date",
            "customfield_11700":"Jefree Lim",
            "customfield_11701":[{"value":"Barry Sage"},{"value":"Colin Ross"},{"value":"Matt Stanton"},{"value":"Russell Davis"},{"value":"Graham Case"}],
            "description":"Patch CUBE DTU OPS servers with latest updates and security from Microsoft",
            "customfield_11702":"Part of Evergreen Project to keep all environment up to date.",
            "customfield_11705":{"value":"Yes"},
            "customfield_11704":{"value":"UAT"},
            "customfield_11709":"CUBEBAUPWK\r\nCUBEDESKTOP05\r\nCUBE-REDACT\r\nCUBE-SANHQ01\r\nCUBE-SRM\r\nCUBETCB01\r\nCUBETCB02\r\nCUBE-VSM02\r\nMGMT-STORE\r\nTCG-SVC01\r\nCUBEDTUSKDN",
            "customfield_11714":"N/A",
            "customfield_11715":"*Backup*\r\n# Shut down the below servers and snapshot\r\n#* CUBEBAUPWK \r\n#* CUBEDESKTOP05 \r\n#* CUBE-REDACT \r\n#* CUBE-SANHQ01 \r\n#* CUBE-SRM \r\n#* CUBETCB01 \r\n#* CUBETCB02 \r\n#* CUBE-VSM02 \r\n#* MGMT-STORE \r\n#* TCG-SVC01 \r\n#* CUBEDTUSKDN\r\n# Power on servers after snapshot\r\n\r\n*Microsoft Updates*\r\n\r\nLog on to VM TCG-SVC01\r\nOpen Windows Server Update Services.\r\nUnder \"All Updates\" Set the Approval field to \"Unapproved\" and the status to \"Failed or Needed\".\r\nSelect all the updates, right click and select approve. Select the computer group Dev.\r\nLog on the VM you are working on and launch windows update.\r\nSelect all the required updates and install.\r\nThe server will probably need a restart depending on the update itself, you should then run windows update again to check all patches have been applied. Once done go through the above steps for the next VM.\r\n\r\n*VMWare Tools update.* \r\n\r\nIn VMWare right click the VM to be updated.\r\nSelect guest and choose \"Install/Upgrade VMware Tools\".\r\nSelect \"Automatic Tools Upgrade\". This will install the tools in the background an will reboot the VM once done.\r\n\r\n*CUBEBAUPWK, CUBE-REPAPP, CUBE-VSM02 update*\r\nLog into the CUBEBAUPWK as root\r\nRun the following command yum update -y\r\nWait for the updates to be applied and reboot the server.\r\nRepeat steps for other servers\r\n\r\nAdd CR details to CFG management form and save as next major version\r\n\r\nRestart servers and check services",
            "customfield_11716":{"value":"Yes"},
            "customfield_11717":"Restore snapshots",
            "customfield_11721":"Matt Stanton\r\nBarry Sage\r\nColin Ross\r\nGraham Case",
            "customfield_11801":" ",
            "customfield_11722":{"value":"No"},
            "customfield_12026":{"value":"No"},
            "customfield_12032":{"value":"Infrastructure"},
            "customfield_12033":[{"value":"Evergreen"}]
       }
    }
"@
)


# Functions
###########

# Logging Function
function Log-It {
    param( [String]$custom )
    if (-not ("log.txt" | Test-Path)) {
        New-Item log.txt -ItemType file
        }
    if ($ErrorResult -ne "") {
            $output = $errorResult
        } elseif ($custom -ne "") {
            $output = $custom
        } elseif ($response -ne "") {
            $output = $response
        } else {
            $output = "No response was recorded but the Log-It function was called"
        }
    (Get-Date -UFormat "%Y-%m-%d %T: Evergreen Patching - ")+$output | Out-File -Append log.txt
    $logString = ""
}

# API Call Function
function Create-Call {
    param( [String]$uri, [String]$method, [String]$issueData, [String]$logString )

    # Clear the response variables
    $response = ""
    $ErrorResult = ""

    # Run the query and catch any error messages
    Try {
    $response = Invoke-RestMethod -Uri $uri -Method $method -Headers $headers -Body $issueData -ContentType $contentType
    } Catch [System.Net.WebException] { $exception = $_.Exception
      $respstream = $exception.Response.GetResponseStream()
      $sr = new-object System.IO.StreamReader $respstream
      $ErrorResult = $sr.ReadToEnd()
      write-host $ErrorResult 
    }
    Log-It -custom $logString
    return $response
}

# Default Values
################

# Set the User details
$username = "support@cube.global"
# While the password is the same, the pass.txt used is different for each machine/account running the script. Regenerate this using the below command
# "password" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | out-file C:\pass.txt
$pass = get-content pass.txt
$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $pass)))
# Create authentication headers
$user    = [System.Text.Encoding]::UTF8.GetBytes("$username"+":"+"$password")
$headers = @{Authorization = "Basic " + [System.Convert]::ToBase64String($user)}
# Set the api calls parameters
$contentType = "application/json"
$url = "https://cube001.atlassian.net"
$endpoint = "/rest/api/2/issue/"
$linkDocument = @"
{ 

        "object": { 
            "url":"https://thecontentgroupltd.sharepoint.com/:x:/r/saas/Shared%20Documents/Private%20Team%20Documents/Infrastructure%20Team/Documentation/Configuration%20Management/Environments/CUBE/CUBE_Evergreen.xlsx?d=wbc51c4b6e5f34ce186339b03b745efe8&csf=1&e=9wkbsi", 
            "title":"CUBE_Evergreen.xlsx"
        } 

}
"@

# Create the Issues
###################

# Create the INF issue
$response = Create-Call -uri $url$endpoint -method "POST" -issueData $inf
# Set the INF issue variables for the CRs
$parentIssue = $response.key

# Transition to In Progress
$transition = @"
{
  "transition": {
    "id": "61"
  }
}
"@
Create-Call -uri $url$endpoint$parentIssue"/transitions" -method "POST" -issueData $transition -logString "$parentIssue was successfully transitioned to In Progress"


$linkParent = @"
{
    "update":{
        "issuelinks":[
            {
            "add":{
                "type":{
                    "name":"Relates",
                    "inward":"relates to",
                    "outward":"relates to"
                },
                "outwardIssue":{
                    "key":"$parentIssue"
                }
            }
            }
        ]
    }
}
"@


# Create the extra environments CR's
foreach ($CR in $CRs) {
    #Create the CR
    $response = Create-Call -uri $url$endpoint -method "POST" -issueData $CR
    $NewCRKey = $response.key
    
    # Link the INF issue
    Create-Call -uri $url$endpoint$NewCRKey -method "PUT" -issueData $linkParent -logString $NewCRKey" was successfully liked to "$parentIssue

    # Link the Evergreen Document
    Create-Call -uri $url$endpoint$NewCRKey"/remotelink" -method "POST" -issueData $linkDocument -logString $NewCRKey" was successfully linked to CUBE_Evergreen.xlsx"
}

# Create the CR's that are stacked (blocking the previous CR)
# Clear/Set the variables
$oldCRkey = ""
$NewCRKey = ""
$count = 0

foreach ($CR in $CRsStacked) {
    # Create the CR
    $response = Create-Call -uri $url$endpoint -method "POST" -issueData $CR
    $NewCRKey = $response.key
    
    # Link the INF issue
    Create-Call -uri $url$endpoint$NewCRKey -method "PUT" -issueData $linkParent -logString $NewCRKey" was successfully linked to "$parentIssue

    # Block the previous CR
    if ($count -gt 0){
        $linkCR = @"
        {
           "update":{
              "issuelinks":[
                 {
                    "add":{
                       "type":{
                          "name":"Blocks",
                          "inward":"is blocked by",
                          "outward":"blocks"
                       },
                       "outwardIssue":{
                          "key":"$NewCRkey"
                       }
                    }
                 }
              ]
           }
        }
"@
        Create-Call -uri $url$endpoint$oldCRkey -method "PUT" -issueData $linkCR -logString $oldCRkey" has successfully blocked "$NewCRKey
    }

    # Link the Evergreen Document
    Create-Call -uri $url$endpoint$NewCRKey"/remotelink" -method "POST" -issueData $linkDocument -logString $NewCRKey" was successfully linked to CUBE_Evergreen.xlsx"
    
    $oldCRkey = $NewCRKey
    $count += 1
}

Log-It -custom "Finished"