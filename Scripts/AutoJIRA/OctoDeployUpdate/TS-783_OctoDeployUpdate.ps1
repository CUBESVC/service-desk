## Get and set the date variables
$date = (Get-Date)
$firstDayOfMonth = Get-Date ((("01/" + (Get-Date $date).Month).ToString() + "/" + ((Get-Date $date).Year).ToString() + " 00:00:00"))
$lastDayOfMonth = (Get-Date ((("01/" + ((($firstDayOfMonth).AddMonths(1)).Month).ToString() + "/" + ((($firstDayOfMonth).AddMonths(1)).Year).ToString())))) - (New-TimeSpan -seconds 1)
$date = Get-Date ($firstDayOfMonth) -UFormat %B%Y
$duedate = Get-Date ($lastDayOfMonth) -UFormat %Y-%m-%d

# Change working directory to the script location
cd $PSScriptRoot

## Get the Octopus Deploy Version
# Set TLS ver for scraping https
[Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11"
# Scrape the downloads page
$response = Invoke-WebRequest -Uri https://octopus.com/downloads
# Get the version details from the html body
$version = ($response.ParsedHtml.body.getElementsByClassName("col-md-4") | select innerText).innerText -replace '(^\s+|\s+$)','' -replace '\s+',' '
# Get the new version number
$ver = $version.Substring($version.IndexOf('Version: '), (($version.IndexOf(' Released:')) -($version.IndexOf('Version: '))))
$verNum = $ver.Substring(9)
# Get the release date
$rel = $version.Substring($version.IndexOf('Released: '), (($version.IndexOf(' What'))-($version.IndexOf('Released: '))))
# Get the download link
$download = "https://download.octopusdeploy.com/octopus/Octopus.$verNum-x64.msi"


## Create the issue metadata
# Master INF Issue
$inf = @"
    {
       "fields":{
          "project":{
             "key":"INF"
          },
          "summary":"Upgrade Octopus Deploy to v$vernum",
          "description":"Monthly task to download the latest Octopus Deploy version of $verNum ($rel) and install.",
          "issuetype":{
             "name":"Request"
          },
          "assignee":{
             "name":"JefreeLim"
          }
       }
    }
"@

# PREPROD and PROD CR's (PROD will be blocked by preprod)
$CRsStacked = @(@"
    {  
       "fields":{  
          "project":{  
             "key":"CR"
          },
          "issuetype":{  
             "name":"Request"
          },
          "duedate":"$duedate",
          "summary":"Upgrade PREPROD Octopus Deploy to v$vernum",
          "customfield_11700":"Jefree Lim",
          "customfield_11701":[  
             {  
                "value":"Barry Sage"
             },
             {  
                "value":"Colin Ross"
             },
             {  
                "value":"Matt Stanton"
             },
             {  
                "value":"Russell Davis"
             },
             {  
                "value":"Graham Case"
             }
          ],
          "description":"Monthly PREPROD update of [Octopus Deploy|https://octopus.com/] as per INF-1003.\r\n$ver\r\n$rel",
          "customfield_11702":"Update Octopus Deploy to the latest version.",
          "customfield_11705":{  
             "value":"No"
          },
          "customfield_11704":{  
             "value":"PREPROD"
          },
          "customfield_11709":"DMZ-CUBEOCTO (10.1.3.90)",
          "customfield_11714":"Octopus.$verNum-x64.msi - $download",
          "customfield_11715":"*Put Octopus Deploy in Maintenance Mode*\r\n# Open web browser and navigate to https://octo.cube.global\r\n# Login with your credentials\r\n# Navigate to Configuration > Maintenance\r\n# Set *Maintenance mode* to ON > Save\r\n\r\n*Snapshot*\r\n# Take a snapshot of the following server: \r\n## DMZ-CUBEOCTO\r\n\r\n*Backup Databases*\r\n# RDP\\Console into DMZ-CUBEOCTO\r\n# Open {{SQL Server Management Studio}}\r\n# Expand {{CUBEOCTO > Databases}}\r\n# Right click on {{Octopus}} database\r\n# Select {{Tasks > Back Up...}}\r\n# Specify the following values:\r\n## Backup Type: Full\r\n## Destination: Disk\r\n# Click remove to remove any paths\r\n# Click add and select C drive, name file: Octopus_yyyymmdd.bak\r\n# Click ok > wait for backup process to complete\r\n\r\n*Backup Octopus Manager Master Key*\r\n# Open Octopus Manager\r\n# Click *View master key* > Copy to clipboard\r\n# Close Octopus Manager\r\n# On the desktop, open *Master Key.txt* > paste master key > save text file\r\n\r\n*Install Octopus Deploy $verNum*\r\n# Open Windows Explorer > navigate to path where you downloaded *Octopus.$verNum-x64.msi*\r\n# In the Setup Wizard, click Next > tick _I accept the terms in the License Agreement_ > Next > Next > Install\r\n# Wait for installation to finish\r\n# *Reconfiguring OctopusServer* window will appear > wait for window to close\r\n\r\n*Refresh Browser*\r\n# Open web browser and navigate to https://octo.cube.global > on the keyboard, press Ctrl+F5\r\n# The updated Octopus Deploy UI will now load\r\n\r\n*Upgrade All Tentacles*\r\n# In Octopus Deploy, navigate to Infrastructure > Environments\r\n# Click *Upgrade All Tentacles*\r\n\r\n*Put Octopus Deploy in Normal Mode*\r\n# In Octopus Deploy, navigate to Configuration > Maintenance > click *DISABLE MAINTENANCE MODE*",
          "customfield_11716":{  
             "value":"Yes"
          },
          "customfield_11717":"1. Revert to snapshot\r\n2. Login to Octopus Deploy and rollback calamari on tentacles",
          "customfield_11721":"Matt Stanton\r\nGraham Case\r\nBarry Sage\r\nColin Ross",
          "customfield_11801":" ",
          "customfield_11722":{  
             "value":"No"
          },
          "customfield_12026":{  
             "value":"No"
          },
          "customfield_12032":{  
             "value":"Infrastructure"
          },
          "customfield_12033":[  
             {  
                "value":"Maintenance"
             }
          ]
       }
    }
"@, @"
    {  
       "fields":{  
          "project":{  
             "key":"CR"
          },
          "issuetype":{  
             "name":"Request"
          },
          "duedate":"$duedate",
          "summary":"Upgrade PROD Octopus Deploy to v$vernum",
          "customfield_11700":"Jefree Lim",
          "customfield_11701":[  
             {  
                "value":"Barry Sage"
             },
             {  
                "value":"Colin Ross"
             },
             {  
                "value":"Matt Stanton"
             },
             {  
                "value":"Russell Davis"
             },
             {  
                "value":"Graham Case"
             }
          ],
          "description":"Monthly PROD update of [Octopus Deploy|https://octopus.com/] as per INF-1003.\r\n$ver\r\n$rel",
          "customfield_11702":"Update Octopus Deploy to the latest version.",
          "customfield_11705":{  
             "value":"No"
          },
          "customfield_11704":{  
             "value":"PROD"
          },
          "customfield_11709":"CUBEOCTO (10.1.3.90)",
          "customfield_11714":"Octopus.$verNum-x64.msi - $download",
          "customfield_11715":"*Put Octopus Deploy in Maintenance Mode*\r\n# Open web browser and navigate to https://octo.cube.global\r\n# Login with your credentials\r\n# Navigate to Configuration > Maintenance\r\n# Set *Maintenance mode* to ON > Save\r\n\r\n*Snapshot*\r\n# Take a snapshot of the following server: \r\n## CUBEOCTO\r\n\r\n*Backup Databases*\r\n# RDP\\Console into CUBEOCTO\r\n# Open {{SQL Server Management Studio}}\r\n# Expand {{CUBEOCTO > Databases}}\r\n# Right click on {{Octopus}} database\r\n# Select {{Tasks > Back Up...}}\r\n# Specify the following values:\r\n## Backup Type: Full\r\n## Destination: Disk\r\n# Click remove to remove any paths\r\n# Click add and select C drive, name file: Octopus_yyyymmdd.bak\r\n# Click ok > wait for backup process to complete\r\n\r\n*Backup Octopus Manager Master Key*\r\n# Open Octopus Manager\r\n# Click *View master key* > Copy to clipboard\r\n# Close Octopus Manager\r\n# On the desktop, open *Master Key.txt* > paste master key > save text file\r\n\r\n*Install Octopus Deploy $verNum*\r\n# Open Windows Explorer > navigate to path where you downloaded *Octopus.$verNum-x64.msi*\r\n# In the Setup Wizard, click Next > tick _I accept the terms in the License Agreement_ > Next > Next > Install\r\n# Wait for installation to finish\r\n# *Reconfiguring OctopusServer* window will appear > wait for window to close\r\n\r\n*Refresh Browser*\r\n# Open web browser and navigate to https://octo.cube.global > on the keyboard, press Ctrl+F5\r\n# The updated Octopus Deploy UI will now load\r\n\r\n*Upgrade All Tentacles*\r\n# In Octopus Deploy, navigate to Infrastructure > Environments\r\n# Click *Upgrade All Tentacles*\r\n\r\n*Put Octopus Deploy in Normal Mode*\r\n# In Octopus Deploy, navigate to Configuration > Maintenance > click *DISABLE MAINTENANCE MODE*",
          "customfield_11716":{  
             "value":"Yes"
          },
          "customfield_11717":"1. Revert to snapshot\r\n2. Login to Octopus Deploy and rollback calamari on tentacles",
          "customfield_11721":"Matt Stanton\r\nGraham Case\r\nBarry Sage\r\nColin Ross",
          "customfield_11801":" ",
          "customfield_11722":{  
             "value":"No"
          },
          "customfield_12026":{  
             "value":"No"
          },
          "customfield_12032":{  
             "value":"Infrastructure"
          },
          "customfield_12033":[  
             {  
                "value":"Maintenance"
             }
          ]
       }
    }
"@
)

# Functions
###########

# Logging Function
function Log-It {
    param( [String]$custom )
    if (-not ("log.txt" | Test-Path)) {
        New-Item log.txt -ItemType file
        }
    if ($ErrorResult -ne "") {
            $output = $errorResult
        } elseif ($custom -ne "") {
            $output = $custom
        } elseif ($response -ne "") {
            $output = $response
        } else {
            $output = "No response was recorded but the Log-It function was called"
        }
    (Get-Date -UFormat "%Y-%m-%d %T: OD Update - ")+$output | Out-File -Append log.txt
    $logString = ""
}

# API Call Function
function Create-Call {
    param( [String]$uri, [String]$method, [String]$issueData, [String]$logString )

    # Clear the response variables
    $response = ""
    $ErrorResult = ""

    # Run the query and catch any error messages
    Try {
    $response = Invoke-RestMethod -Uri $uri -Method $method -Headers $headers -Body $issueData -ContentType $contentType
    } Catch [System.Net.WebException] { $exception = $_.Exception
      $respstream = $exception.Response.GetResponseStream()
      $sr = new-object System.IO.StreamReader $respstream
      $ErrorResult = $sr.ReadToEnd()
      write-host $ErrorResult 
    }
    Log-It -custom $logString
    return $response
}

# Default Values
################

# Set the User details
$username = "support@cube.global"
# the pass.txt used is different for each machine, regenerate this using the below command
# "password" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | out-file C:\pass.txt
$pass = get-content pass.txt
$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $pass)))
# Create authentication headers
$user    = [System.Text.Encoding]::UTF8.GetBytes("$username"+":"+"$password")
$headers = @{Authorization = "Basic " + [System.Convert]::ToBase64String($user)}
# Set the api calls parameters
$contentType = "application/json"
$url = "https://cube001.atlassian.net"
$endpoint = "/rest/api/2/issue/"

# Create the Issues
###################

# Create the INF issue
$response = Create-Call -uri $url$endpoint -method "POST" -issueData $inf

# Set the INF issue variables for the CRs
$parentIssue = $response.key
$linkParent = @"
{
    "update":{
        "issuelinks":[
            {
            "add":{
                "type":{
                    "name":"Relates",
                    "inward":"relates to",
                    "outward":"relates to"
                },
                "outwardIssue":{
                    "key":"$parentIssue"
                }
            }
            }
        ]
    }
}
"@

# Create the CR's that are stacked (blocking the previous CR)
# Clear/Set the variables
$oldCRkey = ""
$NewCRKey = ""
$count = 0

foreach ($CR in $CRsStacked) {
    # Create the CR
    $response = Create-Call -uri $url$endpoint -method "POST" -issueData $CR
    $NewCRKey = $response.key
    
    # Link the INF issue
    Create-Call -uri $url$endpoint$NewCRKey -method "PUT" -issueData $linkParent -logString $NewCRKey" was successfully linked to "$parentIssue

    # Block the previous CR
    if ($count -gt 0){
        $linkCR = @"
        {
           "update":{
              "issuelinks":[
                 {
                    "add":{
                       "type":{
                          "name":"Blocks",
                          "inward":"is blocked by",
                          "outward":"blocks"
                       },
                       "outwardIssue":{
                          "key":"$NewCRkey"
                       }
                    }
                 }
              ]
           }
        }
"@
        Create-Call -uri $url$endpoint$oldCRkey -method "PUT" -issueData $linkCR -logString $oldCRkey" has successfully blocked "$NewCRKey
    }
    
    $oldCRkey = $NewCRKey
    $count += 1
}