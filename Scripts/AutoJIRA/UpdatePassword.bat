@ECHO OFF

REM  NAME: UpdatePassword.bat
REM  VERSION: 1.0
REM  AUTHOR: Leigh Butterworth
REM  ------------------------------------------------------------------------------------------------------------------------------------------------
REM  PURPOSE: This script will update the AutoJIRA subfolders with the current pass.txt in the root folder - this is only for testing the scripts
REM           As the production scripts all use the same pass.txt file in the AutoJIRA root directory
REM  ------------------------------------------------------------------------------------------------------------------------------------------------
REM  INSTRUCTIONS
REM   1. Run the following command in powershell:
REM      "password" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | out-file C:\pass.txt
REM      The "password" required is stored in the SD KeePass under the Cube Support JIRA account
REM   3. Replace the pass.txt file in this folder with the output 
REM   4. Run the batch script
REM  ------------------------------------------------------------------------------------------------------------------------------------------------
REM  NOTE: This will need to be done for each user/machine the script is being tested/run on as the encrypted password is unique to the machine
REM  ------------------------------------------------------------------------------------------------------------------------------------------------

CALL :datetime

IF NOT EXIST logpass.txt (type nul >logpass.txt)
ECHO Starting at %dt% >> logpass.txt
whoami >> logpass.txt

ECHO Getting folders and copying pass.txt... >> logpass.txt
FOR /f "delims=" %%D IN ('dir /a:d /b') DO (
   ECHO %dt% Copying "pass.txt" to %%~D >> logpass.txt
   COPY /b /v /y pass.txt "%%~fD" >> logpass.txt
)
ECHO Done >> logpass.txt


CALL :datetime
ECHO Finished at %dt% >> logpass.txt
ECHO ------------------------------------------------------- >> logpass.txt
pause
EXIT

:datetime
SET dt=%date:~10%-%date:~7,2%-%date:~4,2%__%TIME:~0,8%
SET dt=%dt: =0%
SET dt=%dt:_= %
EXIT /B 0