﻿#########################################################################
# Api call to JIRA to Create issues on the INF board for DR testing     #
#                                                                       #
#                     By Leigh Butterworth                              #
#                           V1.0                                        #
#                                                                       #
#########################################################################
Log-It -custom "Starting"
# Set the working directory
cd $PSScriptRoot

# Set the instructions for the testing
$documentation = "https://thecontentgroupltd.sharepoint.com/:w:/r/saas/_layouts/15/Doc.aspx?sourcedoc=%7B34782158-495B-49B9-BF96-FC0098380C8E%7D&file=Weekly%20DR%20Test%20Procedure.docx&action=default&mobileredirect=true&DefaultItemOpen=1"

# Import the list of environments and select the next one
$environments = Import-Csv "DRTesting/Environments.csv"
if (-not ("DRTesting/counter.txt" | Test-Path)) {
    $counter = 0
    $counter | Out-File "DRTesting/counter.txt"
} else {
    $counter = [int](Get-Content "DRTesting/counter.txt")
    if ($counter -eq ($environments.count - 1)) {
        $counter = 0
        $counter | Out-File "DRTesting/counter.txt"
    } else {
        $counter += 1
        $counter | Out-File "DRTesting/counter.txt"
    }
}
$environment = $environments[$counter]

# Default Values
################

# Set the Date Variables
$date = Get-Date -UFormat %d/%m/%Y
$duedate = Get-Date ((Get-Date).adddays(2)) -UFormat %Y-%m-%d

# Set the API User details
$username = "support@cube.global"
# While the password is the same, the pass.txt used is different for each machine/account running the script. Regenerate this using the below command
# "password" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | out-file C:\pass.txt
$pass = get-content pass.txt
$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $pass)))

# Create API authentication headers
$user    = [System.Text.Encoding]::UTF8.GetBytes("$username"+":"+"$password")
$headers = @{Authorization = "Basic " + [System.Convert]::ToBase64String($user)}

# Set the API calls parameters
$contentType = "application/json"
$url = "https://cube001.atlassian.net"
$endpoint = "/rest/api/2/issue/"

# Functions
###########

# Logging Function
function Log-It {
    param( [String]$custom )
    if (-not ("log.txt" | Test-Path)) {
        New-Item log.txt -ItemType file
        }
    if ($ErrorResult -ne "") {
            $output = "Error: $errorResult"
        } elseif ($custom -ne "") {
            $output = $custom
        } elseif ($response -ne "") {
            $output = "Response: $response"
        } else {
            $output = "No response was recorded but the Log-It function was called"
        }
    (Get-Date -UFormat "%Y-%m-%d %T: DR Testing - ")+$output | Out-File -Append log.txt
    $logString = ""
}

# API Call Function
function Create-Call {
    param( [String]$uri, [String]$method, [String]$issueData, [String]$logString )

    # Clear the response variables
    $response = ""
    $ErrorResult = ""

    # Run the query and catch any error messages
    Try {
    $response = Invoke-RestMethod -Uri $uri -Method $method -Headers $headers -Body $issueData -ContentType $contentType
    } Catch [System.Net.WebException] { $exception = $_.Exception
      $respstream = $exception.Response.GetResponseStream()
      $sr = new-object System.IO.StreamReader $respstream
      $ErrorResult = $sr.ReadToEnd()
      write-host $ErrorResult 
    }
    Log-It -custom $logString
    return $response
}

$linkDocument = @"
{ 

        "object": { 
            "url":"$documentation", 
            "title":"Weekly DR Test Procedure.docx"
        } 

}
"@

$transition = @"
{
  "transition": {
    "id": "61"
  }
}
"@


$shortName = $environment.shortName
$longName = $environment.longName

$inf = @"
{
    "fields":{
        "project":{
            "key":"INF"
        },
        "summary":"$shortName DR Test for $date",
        "description":"Run DR test for $longName.",
        "issuetype":{
            "name":"Request"
        },
        "assignee":{
            "name":"Graham.Case"
        },
        "duedate":"$duedate"
    }
}
"@
$response = Create-Call -uri $url$endpoint -method "POST" -issueData $inf
$NewINFKey = $response.key

Create-Call -uri $url$endpoint$NewINFKey"/remotelink" -method "POST" -issueData $linkDocument -logString $NewINFKey" was successfully linked to Weekly DR Test Procedure.docx"
Create-Call -uri $url$endpoint$NewINFKey"/transitions" -method "POST" -issueData $transition -logString $NewINFKey" was successfully transitioned to In Progress"

Log-It -custom "Finished"