﻿#########################################################################
# Api call to JIRA to Create issues on the TS board for Reporting       #
#                                                                       #
#                     By Leigh Butterworth                              #
#                           V1.0                                        #
#                                                                       #
#########################################################################
# Get the date variables

Log-It -custom "Starting"

$datetime = Get-Date
$date = Get-Date $datetime -UFormat %d/%m/%Y
$duedate = Get-Date -UFormat %Y-%m-%d
[Int]$dow = Get-Date $date| Select-Object -ExpandProperty DayOfWeek
[Int]$month = Get-Date $date | Select-Object -ExpandProperty Month
[Int]$year = Get-Date $date | Select-Object -ExpandProperty Year

$startofmonth = Get-Date $date -day 1 -hour 0 -minute 0 -second 0
if ([int]($startofmonth | Select-Object -ExpandProperty DayOfWeek) -eq 6) {
    $startofmonth = ($startofmonth).AddDays(2)
} elseif ([int]($startofmonth | Select-Object -ExpandProperty DayOfWeek) -eq 0) {
    $startofmonth = ($startofmonth).AddDays(1)
}
$endofmonth = ((Get-Date $date -day 1 -hour 0 -minute 0 -second 0).AddMonths(1).AddSeconds(-1))
if ([int]($endofmonth | Select-Object -ExpandProperty DayOfWeek) -eq 6) {
    $endofmonth = ($endofmonth).AddDays(-1)
} elseif ([int]($endofmonth | Select-Object -ExpandProperty DayOfWeek) -eq 0) {
    $endofmonth = ($endofmonth).AddDays(-2)
}

# Set the working directory
cd $PSScriptRoot

# Functions
###########

# Logging Function
function Log-It {
    param( [String]$custom )
    if (-not ("log.txt" | Test-Path)) {
        New-Item log.txt -ItemType file
        }
    if ($ErrorResult -ne "") {
            $output = $errorResult
        } elseif ($custom -ne "") {
            $output = $custom
        } elseif ($response -ne "") {
            $output = $response
        } else {
            $output = "No response was recorded but the Log-It function was called"
        }
    (Get-Date -UFormat "%Y-%m-%d %T: TS Reporting - ")+$output | Out-File -Append log.txt
    $logString = ""
}

# API Call Function
function Create-Call {
    param( [String]$uri, [String]$method, [String]$issueData, [String]$logString )

    # Clear the response variables
    $response = ""
    $ErrorResult = ""

    # Run the query and catch any error messages
    Try {
    $response = Invoke-RestMethod -Uri $uri -Method $method -Headers $headers -Body $issueData -ContentType $contentType -Verbose
    } Catch [System.Net.WebException] { $exception = $_.Exception
      $respstream = $exception.Response.GetResponseStream()
      $sr = new-object System.IO.StreamReader $respstream
      $ErrorResult = $sr.ReadToEnd()
      write-host $ErrorResult 
    }
    Log-It -custom $logString
    return $response
}

# Default Values
################

# Set the User details
$username = "support@cube.global"
# While the password is the same, the pass.txt used is different for each machine/account running the script. Regenerate this using the below command
# "password" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | out-file C:\pass.txt
$pass = get-content pass.txt
$password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR((ConvertTo-SecureString -String $pass)))
# Create authentication headers
$user    = [System.Text.Encoding]::UTF8.GetBytes("$username"+":"+"$password")
$headers = @{Authorization = "Basic " + [System.Convert]::ToBase64String($user)}
# Set the api calls parameters
$contentType = "application/json"
$url = "https://cube001.atlassian.net"
$endpoint = "/rest/api/2/issue/"

# Get the list of reports
$reports = Import-Csv "TSReports/reports.csv"

# Sort which reports need to be run today
$doReports = @()
[Int]$counter = 0

foreach ($report in $reports) {
    if ($datetime -gt (get-date $report.Due).AddMinutes(-5) -AND (Get-Date) -lt (get-date $report.Due).AddMinutes(5)) {
        # Get Daily Reoccuring
        if ($report.Month -eq "0" -AND $report.Week -eq "0" -AND $report.Day -eq "0"){
            # Check if it is due to run
            if ($report.Count -eq $report.FrequencyOffset) {
                $doReports += $report 
                [int]$reports[($counter)].Count = 0
            } else {
                [int]$reports[($counter)].Count += 1
            }
        }
    
        # Get all the weekly reoccuring
        if ($report.Month -eq "0" -AND $report.Week -eq "0"){
            # Get reports with no offset (every week)
            if ($report.FrequencyOffset -eq "0" -AND $report.Day -eq $dow){
                # Check if it is due to run
                if ($report.Count -eq $report.FrequencyOffset) {
                    $doReports += $report 
                    [int]$reports[($counter)].Count = 0
                } else {
                    [int]$reports[($counter)].Count += 1
                }
            }
            # Get reports with offset
            if ($report.FrequencyOffset -ne "0" -AND $report.Day -eq $dow){
                # Check if it is due to run
                if ($report.Count -eq $report.FrequencyOffset) {
                    $doReports += $report 
                    [int]$reports[($counter)].Count = 0
                } else {
                    [int]$reports[($counter)].Count += 1
                }
            }
        }

        # Get all the monthly reoccuring
        # Get reports for day
        $reportDay = $report.Day
        if ($report.Month -eq "0" -AND $report.Week -eq ""){
            if ((get-date (get-date $reportDay/$month/$year) -UFormat %d/%m/%Y) -eq $date){
                # Check if it is due to run
                if ($report.Count -eq $report.FrequencyOffset) {
                    $doReports += $report 
                    [int]$reports[($counter)].Count = 0
                } else {
                    [int]$reports[($counter)].Count += 1
                }
            }
        }
        # Get reports for week/day
        if ($report.Month -eq "0"){
            if ($report.Week -eq "-1"){
                if ($reportDay -eq "-1"){
                    if ($date -eq (get-date $endofmonth -UFormat %d/%m/%Y)){
                        # Check if it is due to run
                        if ($report.Count -eq $report.FrequencyOffset) {
                            $doReports += $report 
                            [int]$reports[($counter)].Count = 0
                        } else {
                            [int]$reports[($counter)].Count += 1
                        }
                    }
                } elseif ($reportDay -eq "0"){
                    if ($date -eq (get-date $endofmonth.AddDays(-7) -UFormat %d/%m/%Y)){
                        # Check if it is due to run
                        if ($report.Count -eq $report.FrequencyOffset) {
                            $doReports += $report 
                            [int]$reports[($counter)].Count = 0
                        } else {
                            [int]$reports[($counter)].Count += 1
                        }
                    }
                } elseif ($reportDay -eq $dow){
                    if ($date -gt (get-date $endofmonth.AddDays(-7) -UFormat %d/%m/%Y)){
                        # Check if it is due to run
                        if ($report.Count -eq $report.FrequencyOffset) {
                            $doReports += $report 
                            [int]$reports[($counter)].Count = 0
                        } else {
                            [int]$reports[($counter)].Count += 1
                        }
                    }
                }
            }
            if ($report.Week -eq "1"){
                if ($reportDay -eq "-1"){
                    if ($date -eq (get-date $startofmonth.AddDays(7) -UFormat %d/%m/%Y)){
                        # Check if it is due to run
                        if ($report.Count -eq $report.FrequencyOffset) {
                            $doReports += $report 
                            [int]$reports[($counter)].Count = 0
                        } else {
                            [int]$reports[($counter)].Count += 1
                        }
                    }
                } elseif ($reportDay -eq "0"){
                    if ($date -eq (get-date $startofmonth -UFormat %d/%m/%Y)){
                        # Check if it is due to run
                        if ($report.Count -eq $report.FrequencyOffset) {
                            $doReports += $report 
                            [int]$reports[($counter)].Count = 0
                        } else {
                            [int]$reports[($counter)].Count += 1
                        }
                    }
                } elseif ($reportDay -eq $dow){
                    if ($date -lt (get-date $startofmonth.AddDays(7) -UFormat %d/%m/%Y)){
                        # Check if it is due to run
                        if ($report.Count -eq $report.FrequencyOffset) {
                            $doReports += $report 
                            [int]$reports[($counter)].Count = 0
                        } else {
                            [int]$reports[($counter)].Count += 1
                        }
                    }
                }
            } 
        }
        $counter +=  1
    }
}

$reports | Export-Csv "TSReports/reports.csv" -notypeinformation

# Clear the Report variable incase of issues
$report = ""

# Go through each report in doReports and create the issues
foreach ($report in $doReports) {
    
        # Create the Report
        $name = $report.Name
        $instructions = $report.Instructions
        $mailto = $report.To
        $description = $instructions+"\r\n\r\n[Send the finished report|mailto:"+$mailto+"]"        
        $location = $report.Location
        $ts = @"
            {
               "fields":{
                  "project":{
                     "key":"TS"
                  },
                  "summary":"$name for $date",
                  "description":"$description",
                  "issuetype":{
                     "name":"Request"
                  },
                  "assignee":{
                     "name":"servicedesk"
                  },
                  "duedate":"$duedate"
               }
            }
"@
        $response = Create-Call -uri $url$endpoint -method "POST" -issueData $ts

}

Log-It -custom "Finished"